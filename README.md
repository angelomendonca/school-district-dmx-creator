## School District Data MX (Initial Data) Generator

The problem was to generated a Data MX file (XML File) from a XLS file that had all the counties. Earlier it was proposed  to do this 
manually as was always being done at the HP project. I figured I could automate this. 

The intended Output structure is as follows:

``` 
	<row>
        <attribute name="SCHOOLDISTRICTID">
            <value>434052</value>
        </attribute>
        <attribute name="NAME">
            <value>ABSECON CITY</value>
        </attribute>
        <attribute name="COUNTY">
            <value>ATLANTIC</value>
        </attribute>
        <attribute name="STATE">
            <value>New Jersey</value>
        </attribute>
        <attribute name="UPPERNAME">
            <value/>
        </attribute>
        <attribute name="VERSIONNO">
            <value>1</value>
        </attribute>
    </row>

```
Manually one would need to create 620 such rows. I wrote A Python Script to read the XLS file using xlrd and output the corresponding xml structure. It took me 10 minutes to complete the task with my knowledge of Python in 2011. 

