#!/usr/bin/env python

import os
import re
import random
import string
import xlrd

__author__ = "Angelo Mendonca"

__version__ = "1.0"

'''
 codetable: \ejbserver\components\cass\codetable\ ct_cassschooldistrict.ctx
there is an ootb, schooldistrict.dmx under ejbserver\components\core\data\demo
use it for xml attributes and use code table for data. most of the fields are nullable in this table.

 <row>
    <attribute name=\"schooldistrictid\"><value>1004</value>    </attribute>    <attribute name=\"name\"><value>athens city schools</value>
    </attributeattribute name=\"county\"><value>usct1</value>    </attribute>    <attribute name=\"state\">
<value>state32</value/attribute>    <attribute name=\"uppername\"><value>athens city schools</value>    </attribute>
    <attribute name=\"versionno\"><value>1</value>    </attribute>  </row>

	pretty print xml -pp on cygwin command line file
'''

school_district_county = {}


def load_county_for_school_district():

    workbook = xlrd.open_workbook('SD.xlsx')
    worksheet = workbook.sheet_by_name('Sheet1')
    num_rows = worksheet.nrows - 1
    num_cells = worksheet.ncols - 1
    curr_row = -1
    while curr_row < num_rows:
        curr_row += 1
        school_district = worksheet.cell_value(curr_row, 3)
        county_school_district = worksheet.cell_value(curr_row, 4)
        school_district_county[school_district] = county_school_district


def printdmx():

    __xmlheader = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"

    __xmlelement = "<row><attribute name=\"schooldistrictid\"><value>insert_generated_id</value></attribute><attribute name=\"name\">" + \
        "<value>insert_code_table_value</value></attribute><attribute name=\"county\"><value>insert_county_code</value>" + \
        "</attribute><attribute name=\"state\"><value>new jersey</value></attribute><attribute name=\"uppername\">" + \
        "<value></value></attribute><attribute name=\"versionno\"><value>1</value></attribute></row>"

    filename = "SchoolDistrict.list"
    id_number = 434052

    with open(filename, "r") as FILE:

        district_file = FILE.readlines()

        for line in district_file:
            line = line.rstrip('\n')
            __xmlelement_first_pass = re.sub(
                r'insert_code_table_value', line, __xmlelement)

            __xmlelement_second_pass = re.sub(
                r'insert_generated_id', str(id_number), __xmlelement_first_pass)

            __xmlelement_third_pass = re.sub(
                r'insert_county_code',
                school_district_county[line],
                __xmlelement_second_pass)

            id_number = id_number + 1
            print __xmlelement_third_pass, "\n"


if __name__ == '__main__':

    load_county_for_school_district()
    printdmx()
